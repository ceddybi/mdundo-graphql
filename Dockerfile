FROM mhart/alpine-node:10 AS buildH
WORKDIR /api
ADD . .
RUN ls
RUN npm install --production
RUN npm run build

FROM mhart/alpine-node:base-10
WORKDIR /api
COPY --from=buildH /api .
EXPOSE 3000
ENV PORT 3000
ENV NODE_ENV production
CMD ["node", "build/index.js"]
