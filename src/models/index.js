// Import the necessary modules.
import { composeWithMongoose, composeWithRelay } from '../schemaComposer';
import Movie from './movie';
import Show from './show';
import { SongTC as SongTCOG, ArtistTC as ArtistTCOG } from './music';

export const ArtistTC = ArtistTCOG;
export const SongTC = SongTCOG;
export const MovieTC = composeWithRelay(composeWithMongoose(Movie));
export const ShowTC = composeWithRelay(composeWithMongoose(Show));

const newSort = MovieTC.getResolver('connection').addFilterArg({
  name: 'newMovies',
  type: 'Boolean!',
  description: '2018 movie filter',
  query: async (query, value, resolveParams) => {
    query.year = '2018';
  },
});

MovieTC.setResolver('connection', newSort);
