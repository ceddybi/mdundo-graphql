/* @flow */
import { isEmpty, compact } from 'lodash';
import { ArtistModel, SongModel } from '.';

type ArtistType = {
  name: String,
  photo: String,
  url: String, // Url to profile
};

type SongType = {
  title: String,
  url: String,
  url_page: String,
  genre: String,
  artistId: String,
};

type ArrayProps = { ary: any, obj: any, key: string };

const addToArray = (props: ArrayProps) => {
  const { obj, key, ary } = props;
  if (isEmpty(ary)) {
    return [{ ...obj }];
  }
  return ary.map(ar => (ar[key] === obj[key] ? { ...ar, ...obj } : ar));
};

export const SaveArtist = async (artist: ArtistType) => {
  try {
    const artistOld = await ArtistModel.findOne({ name: artist.name });

    const profileObj = {
      source: artist.source || 'mdundo',
      url: artist.url,
    };

    if (artistOld) {
      console.log('Artist already exists, updating');
      artistOld.profiles = addToArray({
        ary: artistOld.profiles,
        obj: profileObj,
        key: { name: 'source', value: profileObj.source },
      });
      return await artistOld.save();
    }

    // create a new one here
    const newArtist = new ArtistModel({
      name: artist.name,
      photo: artist.photo,
      profiles: [profileObj],
    });

    return await newArtist.save();
  } catch (error) {
    console.log('Error', error);
    return false;
  }
};

export const SaveSong = async (song: SongType) => {
  try {
    const query = { name: song.name, artistId: song.artistId };
    let songsOld = await SongModel.findOne(query);

    const mp3Obj = {
      url: song.url,
      source: song.source || 'mdundo',
      source_page: song.url_page,
    };

    if (!songsOld) {
      console.log('creating new songs');
      // create a new one here
      const newSong = new SongModel({
        genre: song.genre,
        name: song.name,
        mp3s: [mp3Obj],
        artistId: song.artistId,
      });
      await newSong.save();
      return true;
    }
    console.log('Song already exists, updating');

    songsOld = songsOld.toObject();

    const mp3sNew = await addToArray({
      ary: compact(songsOld.mp3s),
      obj: mp3Obj,
      key: 'source',
    });

    await SongModel.findOneAndUpdate(query, { mp3s: mp3sNew });

    // const newMp3 = songsOld.mp3s.map(mp3 => (mp3.source === mp3Obj.source ? mp3Obj : mp3));
    // console.log('MP3s', JSON.stringify(mp3sNew));
    // console.log('MP3s', JSON.stringify(mp3sNew));

    // songsOld.mp3s = mp3sNew;
    // await songsOld.save();
    // await SongModel.updateOne({ _id: songsOld._id }, { mp3s: mp3sNew });
    return true;
  } catch (error) {
    console.log('Error', error);
    return false;
  }
};
