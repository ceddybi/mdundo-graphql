/* @flow */

import mongoose from 'mongoose';
import { Resolver } from 'graphql-compose';
import { composeWithMongoose, composeWithRelay } from '../../schemaComposer';
import { GraphQLMongoID } from 'graphql-compose-mongoose/node8';
/**
 * Index: _id, -> deleted, available
 * Artist: name, photo, cover
 * Song: name, photo, title, artistName, albumId, albumArtist: [string], lyrics
 */

const userDefaultIndex = {
  // Bio Data
  bio: String,
  born: Date,
  firstname: String,
  lastname: String,
  username: String,

  // Role
  role: String,

  //
};

const defaultIndex = {
  name: String,
  deleted: { type: Boolean, default: false },
  deletedAt: Date,
  available: { type: Boolean, default: true },
};

/**
 * ARTIST
 */
const ArtistProfilesSchema = new mongoose.Schema(
  {
    url: String,
    source: String,
  },
  {
    _id: false, // disable `_id`
  }
);

const ArtistSchema = new mongoose.Schema(
  {
    ...defaultIndex,
    ...userDefaultIndex,
    photo: String,
    cover: String,
    profiles: [ArtistProfilesSchema],
  },
  { collection: 'artists', timestamps: true }
);

export const ArtistModel = mongoose.model('Artist', ArtistSchema);
export const ArtistTC = composeWithRelay(composeWithMongoose(ArtistModel));

/**
 * SONG
 */
const SongSourceSchema = new mongoose.Schema(
  {
    url: String,
    source: String,
    source_page: String,
  },
  {
    _id: false, // disable `_id`
  }
);
const SongSchema = new mongoose.Schema(
  {
    ...defaultIndex,
    photo: String,
    mp3s: [SongSourceSchema],
    artistId: String,
    albumId: String,
    albumName: String,
    genre: [String],
    year: String,
    lyrics: String,
  },
  { collection: 'songs', timestamps: true }
);

export const SongModel = mongoose.model('Song', SongSchema);
export const SongTC = composeWithRelay(composeWithMongoose(SongModel));

/**
 SongTC.addRelation('artist', {
  resolver: ArtistTC.get('$findById'),
  prepareArgs: {
    _id: source => new mongoose.Types.ObjectId(source.artistId),
  },
  projection: { _id: true },
});
 

ArtistTC.addRelation('songs', {
  resolver: SongTC.get('$findMany'),
  prepareArgs: {
    artistId: source => new mongoose.Types.ObjectId(source._id),
  },
  projection: { _id: true },
});


*/

// Get songs by artist id
ArtistTC.addResolver(
  new Resolver({
    name: 'songsByArtistId',
    kind: 'query',
    type: [SongTC],
    args: {
      id: {
        type: GraphQLMongoID,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { id } = args;
      try {
        const songs = await SongModel.find({ artistId: id }).exec();
        return songs;
      } catch (error) {
        console.error(error);
        return [];
      }
    },
  })
);
