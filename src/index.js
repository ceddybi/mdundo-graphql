/* @flow */

import express from 'express';
import cors from 'cors';
import graphqlHTTP from 'express-graphql';
import expressPlayground from 'graphql-playground-middleware-express';
import { expressPort } from './config';
import './mongooseConnection';
import musicSchema from './schema';

const url = `/api`; // eslint-disable-line

const server = express();
server.use(cors());

// $FlowFixMe
server.get('/', (req, res) => {
  res.status(200).json({});
});

server.listen(expressPort, () => {
  console.log(`The server is running at http://localhost:${expressPort}/`);
});

server.use(
  url,
  graphqlHTTP(() => ({
    schema: musicSchema,
    graphiql: true,
    formatError: error => ({
      message: error.message,
      stack: !error.message.match(/for security reason/i) ? error.stack.split('\n') : null,
    }),
  }))
);
server.get(`${url}-playground`, expressPlayground({ endpoint: url }));
