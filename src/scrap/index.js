/* eslint-disable prettier/prettier */
import axios from 'axios';
import cheerio from 'cheerio';
import { find } from 'lodash';
import { SaveArtist, SaveSong } from '../models/musicSaver';
import { ArtistModel } from '../models/music';

function genCharArray(charA, charZ) {
  const a = [];

  let i = charA.charCodeAt(0);

  const j = charZ.charCodeAt(0);
  for (; i <= j; ++i) {
    a.push(String.fromCharCode(i));
  }
  return a;
}

export const scrap = async () => {
  const artistPage = 'http://mdundo.com/artists/all/';
  const letters = genCharArray('a', 'z');

  try {
    letters.forEach(async (leta, index) => {
      const page = `${artistPage}${leta}`;
      const results = await axios.get(page);
      if (results) {
        console.log('done fetching');
        const htmlData = results.data;
        const $ = cheerio.load(htmlData);
        // Save artists and pull songs maybe
        const artists = await findArtistDetails($);
        await Promise.all(artists.map(art => SaveArtist(art)));
      }
    });

    // scrap and save here
  } catch (error) {
    console.log('Error', error);
  }
};

/** FIND ALL Artists and add songs */
export const scrapSongs = async () => {
  try {
    const artists = await ArtistModel.find({ profiles: { $exists: true, $ne: [] } }); // .limit(100);

    await Promise.all(
      artists.map(async (artist, index) => {

        const mdundoSource = find(artist.profiles, { source: 'mdundo' });

        if(mdundoSource && mdundoSource.url){
            const artistPage = mdundoSource.url;
            // console.log('Got the artist url', artistPage);

            const results = await axios.get(artistPage);
            if (results) {
                // console.log('Got results ', artistPage);
                const htmlData = results.data;
                const $ = cheerio.load(htmlData);
                // Save artists and pull songs maybe
                const songs = await getSongsFromArtistPage($);
                await Promise.all(songs.map(song => SaveSong({...song, artistId: artist._id, url_page: artistPage })));
            }
            
        }
        
      })
    );
    return;
  } catch (error) {
    console.log('Error', error);
  }
};

// Page with artists

// FOR EACH ARTIST
// Go to there page and save the song

/**
 * E.g http://mdundo.com/a/12051
 * Get songs from artist page
 * @param {*} $
 *
 */
const getSongsFromArtistPage = $ => {
  const songList = $('#song_list');
  const songs = [];
  songList.find($('li')).each((index, element) => {
    const songUrl = $(element)
      .find($('a.btn.btn-download'))
      .attr('href');
    const songTitle = $(element)
      .find($('a.song-info > span.song-title'))
      .text();
    const artistName = $(element)
      .find($('a.song-info > span.song-author'))
      .text();
    const genre = $(element)
      .find($('div.song-genre > a'))
      .text();
    const genreLink = $(element)
      .find($('div.song-genre > a'))
      .attr('href');
    const songTime = $(element)
      .find($('span.song-time.stream_off'))
      .text();

    songs.push({ time: songTime, url: songUrl, name: songTitle, artistName, genre });
  });
  return songs;
};

/** URLS = http://mdundo.com/artists//ug
 * http://mdundo.com/artists/all/a
 * Get artistUrl, artistName, artistPicture
 * @param {*} $
 */
const findArtistDetails = async $ => {
  const artists = [];
  $('.artist .artist_thumbnail').each((index, element) => {
    const picture = $(element).find($('.artist_picture img'));
    const artistName = picture.attr('alt');
    const artistPicture = picture.attr('src');

    const artistUrl = $(element)
      .find($('a'))
      .attr('href');
    artists.push({ name: artistName, photo: artistPicture, url: artistUrl });
  });

  return artists;
};
