/* @flow */

import path from 'path';
import fs from 'fs';

export const expressPort = process.env.PORT || 3000;
export const mongoUri =
  process.env.MONGODB_URI || 'mongodb://localhost:27017/graphql-compose-mongoose';
export const examplesPath = './examples';