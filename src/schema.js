/* @flow */

import { schemaComposer, composeWithRelay } from './schemaComposer';
import { ArtistTC, SongTC, MovieTC, ShowTC } from './models';

composeWithRelay(schemaComposer.Query);

schemaComposer.Query.addFields({
  // Movies
  movieConnection: MovieTC.getResolver('connection'),
  showConnection: ShowTC.getResolver('connection'),
  moviePagination: MovieTC.getResolver('pagination'),
  showPagination: ShowTC.getResolver('pagination'),

  movieById: MovieTC.get('$findById'),
  showById: ShowTC.get('$findById'),

  // Music
  songConnection: SongTC.getResolver('connection'),
  artistConnection: ArtistTC.getResolver('connection'),
  songPagination: SongTC.getResolver('pagination'),
  artistPagination: ArtistTC.getResolver('pagination'),

  artistById: ArtistTC.get('$findById'),
  songById: SongTC.get('$findById'),
  songsByArtistId: ArtistTC.getResolver('songsByArtistId'),
});

schemaComposer.Mutation.addFields({
  artistCreate: ArtistTC.getResolver('createOne'),
  artistCreateMany: ArtistTC.getResolver('createMany'),
});

const graphqlSchema = schemaComposer.buildSchema();
export default graphqlSchema;
